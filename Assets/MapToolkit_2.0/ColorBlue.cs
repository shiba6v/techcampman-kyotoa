﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ColorBlue : MonoBehaviour {

	public bool buttonClicked = false;
	private RawImage _def;
	private TextureBool _texturebool;

	void Start(){
		_texturebool = transform.GetComponent<TextureBool> ();
	}


	public void ChangeBlue(){

		if (buttonClicked == false && _texturebool.MasterBool == false) {
//			_def.color = this.GetComponent<RawImage> ().color;
			Color _blue = new Color(0.295f, 0.295f, 0.295f, 1.0f);
			this.GetComponent<RawImage> ().color = _blue;
			buttonClicked = !buttonClicked;
			_texturebool.MasterBool = true;
		} 

		else if (buttonClicked == true ) {
			this.GetComponent<RawImage> ().color = Color.white;
			buttonClicked = !buttonClicked;
			_texturebool.MasterBool = false;
		}
	}
}
