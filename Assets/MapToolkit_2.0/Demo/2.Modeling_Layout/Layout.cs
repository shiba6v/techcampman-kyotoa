﻿using UnityEngine;
using System.Collections;

public class Layout : MonoBehaviour
{
	public Transform targetObj;
	private Manager mg;
	private double[] targetCoordinate;
	
	void Awake ()
	{
		Zoom.OnZoomState += OnZoomState;		
		Zoom.OnRefreshMap += OnRefreshMap;
		
		mg = GameObject.Find ("Manager").GetComponent<Manager> ();

	}
	
	void OnEnable ()
	{
		targetObj = GameObject.Find ("building").transform;
	}

	void Update ()
	{
		if (targetObj != null)
			targetCoordinate = mg.PostoGIS (targetObj.position);
	}

	public void OnZoomState (float deltaScale)
	{
		targetObj.position = mg.GIStoPos (targetCoordinate);  //Position
		targetObj.transform.localScale *= deltaScale;
	}

	void OnRefreshMap (Vector3 initiCamPos)
	{
		targetObj.transform.position -= new Vector3 (initiCamPos.x, 0, initiCamPos.z);
	}
}
