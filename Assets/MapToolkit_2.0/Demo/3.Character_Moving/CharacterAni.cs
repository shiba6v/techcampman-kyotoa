﻿using UnityEngine;
using System.Collections;

public class CharacterAni : MonoBehaviour
{


	private ArrayList locationList;
	private ArrayList locationListScreen;
	private int iIndex = 0;
	private Transform target ;
	private bool zoomChange;
	public float speed = 1f;
	private float originSpeed;
	private int  zoomLevel;
	private float scaleDelta;
	private string lineParameter;
	public float  characterScale = 20;
	public bool ani = false;
	Manager mg;

	// Use this for initialization
	void Start ()
	{
		Zoom.OnChangeZoom += OnChangeZoom;	
		mg = GameObject.Find ("Manager").GetComponent<Manager> ();
		if (ani) {
            Animation animation = GetComponent<Animation>();
			animation.wrapMode = WrapMode.Loop;
			animation ["WIN"].wrapMode = WrapMode.ClampForever;
		}
		scaleDelta = transform.localScale.x * characterScale;
		zoomLevel = mg.sy_Map.zoom;
		originSpeed = speed;
		locationList = new ArrayList ();
		
		locationListScreen = new ArrayList ();

		
		locationList.Add (new double[] { 37.5670266,126.977266 });
		locationList.Add (new double[] { 37.5669798,126.9786755});
		locationList.Add (new double[] {37.566922,126.9791702 });
		locationList.Add (new double[] { 37.5661048,126.979151 });	
		locationList.Add (new double[] { 37.5659325,126.9790713 });	
		locationList.Add (new double[] { 37.5656274,126.9788519 });
		locationList.Add (new double[] { 37.5652853,126.9786603 });
		locationList.Add (new double[] { 37.5649282,126.9785262 });
		locationList.Add (new double[] { 37.5648994,126.9772905 });
		locationList.Add (new double[] { 37.5652705,126.9773308 });
		
		//close
		locationList.Add (new double[] { 37.5670266,126.977266 });
		lineParameter = "&path=color:0xff0030";
		
		for (int i=0; i< locationList.Count; i++) {
			double[] d_pos = (double[])locationList [i];
			lineParameter += "|" + d_pos [0].ToString () + "," + d_pos [1].ToString ();
			double[] point = {(double)d_pos [1],  (double)d_pos [0]};
			Vector3 pos = mg.GIStoPos (point);  
			locationListScreen.Add (pos);
		}
		 #if !(UNITY_IPHONE)
		mg.sy_Map.addParameter = lineParameter;
		 #endif
		GameObject AniTarget = new GameObject ("AniTarget");
		AniTarget.transform.position = Vector3.zero;
		target = AniTarget.transform;
		transform.position = (Vector3)locationListScreen [0];
		target.position = (Vector3)locationListScreen [1];
		transform.LookAt (target); 
		float scalefactor = Mathf.Pow (2f, mg.sy_Map.zoom - 18);

		transform.localScale = new Vector3 (scalefactor * scaleDelta, scalefactor * scaleDelta, scalefactor * scaleDelta);

	}

	double[] oldPos;

	void reCalCoordinate ()
	{
		locationListScreen = new ArrayList ();
		for (int i=0; i< locationList.Count; i++) {
			
			double[] d_pos = (double[])locationList [i];
			double[] invers_pos = new double[]{d_pos [1], d_pos [0]};
			Vector3 pos3 = mg.GIStoPos (invers_pos);
			
			locationListScreen.Add (pos3);
			
		}
		transform.position = (Vector3)locationListScreen [iIndex];
		target.position = (Vector3)locationListScreen [iIndex];
		zoomChange = false;

		float scalefactor = Mathf.Pow (2f, mg.sy_Map.zoom - 18);
		transform.localScale = new Vector3 (scalefactor * scaleDelta, scalefactor * scaleDelta, scalefactor * scaleDelta);
		
		if (zoomLevel - mg.sy_Map.zoom > 0) {
			speed = originSpeed / Mathf.Pow (2f, Mathf.Abs (zoomLevel - mg.sy_Map.zoom)); 
		} else if (zoomLevel - mg.sy_Map.zoom < 0) {
			speed = originSpeed * Mathf.Pow (2f, Mathf.Abs (zoomLevel - mg.sy_Map.zoom)); 
		} else {
			speed = originSpeed;
		}
	}

	void OnChangeZoom ()
	{

		reCalCoordinate ();
	}
	
	void Update ()
	{	
		if (zoomChange)
			return;
		float step = speed * Time.deltaTime;
		
		if (Vector3.Distance (transform.position, target.position) == 0.0f) {
			if (iIndex < locationListScreen.Count - 1) {
				iIndex++;
				
			} else {
				iIndex = 0;
			}
			
			target.position = (Vector3)locationListScreen [iIndex];
			transform.LookAt (target); 
			
			if (ani) {
				if (iIndex == 3 || iIndex == 9) {
                    GetComponent<Animation>().Play ("WIN");
				
					zoomChange = true;
				
					StartCoroutine (wait_ani ());	
				}
			}
		}

		transform.position = Vector3.MoveTowards (transform.position, target.position, step);		
	}
	
	private IEnumerator wait_ani ()
	{
		yield return new WaitForSeconds(1.5f);	
		zoomChange = false;
        GetComponent<Animation>().Play ("run");
	}

}
