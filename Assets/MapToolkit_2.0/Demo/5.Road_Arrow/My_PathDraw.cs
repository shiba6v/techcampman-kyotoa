using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class My_PathDraw : MonoBehaviour
{

	
	public Transform[] roadPoints;
	public bool generatePathObject = true;
	public bool showGizmo = true;
	public float pathLineWidth = 0.01f;
	public Material pathLineMaterial;
	private Transform thisT;
	public  ArrayList roadPointsList = new ArrayList ();
	public ArrayList locationList;
	public ArrayList locationListScreen;
	private My_PathIndicator PathIndicator1;
	Manager mg;
	
	void Awake ()
	{
		thisT = transform;
	}

	void OnChangeZoom ()
	{
		RegenLinePath ();
	}

	void Start ()
	{
		
		mg = GameObject.Find ("Manager").GetComponent<Manager> (); 
		Zoom.OnChangeZoom += OnChangeZoom;
		locationList = new ArrayList ();
		locationListScreen = new ArrayList ();
		
		locationList.Add (new double[] { 37.5670073383448,126.977275954357 });
		locationList.Add (new double[] { 37.5669798,126.9786755});
		locationList.Add (new double[] {37.5669231783131,126.979190034117 });
		locationList.Add (new double[] { 37.5661048,126.979151 });	
		locationList.Add (new double[] { 37.5659325,126.9790713 });	
		locationList.Add (new double[] { 37.5656274,126.9788519 });
		locationList.Add (new double[] { 37.5652853,126.9786603 });
		locationList.Add (new double[] { 37.5649282,126.9785262 });
		locationList.Add (new double[] { 37.5649017272091,126.97732161101 });
		locationList.Add (new double[] { 37.5652705,126.9773308 });

		for (int i=0; i< locationList.Count; i++) {

			double[] d_pos = (double[])locationList [i];
			double[] point = {(double)d_pos [1],  (double)d_pos [0]};
			Vector3 pos = mg.GIStoPos (point);
			locationListScreen.Add (pos);
			
		}



		if (generatePathObject) {
			CreateLinePath ();
		}
	}

	void CreateLinePath ()
	{
		roadPointsList = new ArrayList ();
		for (int i=0; i< locationListScreen.Count; i++) {
			GameObject tempPoint = new GameObject ("RoadPoint" + i);
			tempPoint.transform.parent = gameObject.transform;
            tempPoint.AddComponent<Map_DebugShowSelf>();
			Vector3 d_pos = (Vector3)locationListScreen [i];
			d_pos.y = 0.0011f;
			tempPoint.transform.position = d_pos;
			Transform wayPoint = tempPoint.transform;
			roadPointsList.Add (wayPoint);
		}


		Vector3 offsetPos = new Vector3 (0, 0, 0);

		for (int i=1; i<roadPointsList.Count; i++) {
				
			GameObject obj = new GameObject ();
			obj.name = "path" + i.ToString ();

			Transform objT = obj.transform;
			objT.parent = thisT;

			LineRenderer line = obj.AddComponent<LineRenderer> ();

			line.material = pathLineMaterial;
			line.SetWidth (pathLineWidth, pathLineWidth);

			Transform wayPoint3 = (Transform)roadPointsList [i - 1];
			Transform wayPoint4 = (Transform)roadPointsList [i];

			line.SetPosition (0, wayPoint3.position + offsetPos);
			line.SetPosition (1, wayPoint4.position + offsetPos);
		}
	}

	public	void RegenLinePath ()
	{
		for (int k=0; k<GameObject.Find("RoadArrow").transform.childCount; k++) {	
			GameObject item = GameObject.Find ("RoadArrow").transform.GetChild (k).gameObject;
			GameObject.Destroy (item);

		}
		
		locationListScreen = new ArrayList ();

		for (int i=0; i< locationList.Count; i++) {
			double[] d_pos = (double[])locationList [i];
			double[] point = {(double)d_pos [1],  (double)d_pos [0]};
			Vector3 pos = mg.GIStoPos (point);
			locationListScreen.Add (pos);
		}
		
		if (generatePathObject) {
			CreateLinePath ();
		}
		return;
	}

	public Transform[] GetPath ()
	{
		for (int i=1; i<roadPointsList.Count; i++) {
			Transform wayPoint = (Transform)roadPointsList [i];
			Vector3 temp = wayPoint.position;
			temp.y += 0.01f;
			wayPoint.position = temp;
			roadPointsList [i] = wayPoint;
		}
		Transform[] array = new Transform[roadPointsList.Count];
		roadPointsList.CopyTo (array);
		return array;
	}
	
	void OnDrawGizmos ()
	{
		if (showGizmo) {
			Gizmos.color = Color.blue;
			if (roadPoints != null && roadPoints.Length > 0) {
				for (int i=1; i<roadPoints.Length; i++) {
					if (roadPoints [i - 1] != null && roadPoints [i] != null)
						Gizmos.DrawLine (roadPoints [i - 1].position, roadPoints [i].position);
				}
			}
		}
	}
	
}

