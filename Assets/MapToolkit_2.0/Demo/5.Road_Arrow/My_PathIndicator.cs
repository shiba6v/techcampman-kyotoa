using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class My_PathIndicator : MonoBehaviour
{

	private ParticleSystem indicator;
	private Transform indicatorT;
	public Transform indicatorTest;
	public float stepDist = 1;
	public float updateRate = 0.1f;
	private int wpCounter = 0;
	private My_PathDraw path;
	public bool run = true;
	private Manager mg;

	void Start ()
	{
		mg = GameObject.Find ("Manager").GetComponent<Manager> ();
		Zoom.OnChangeZoom += OnChangeZoom;
		StartCoroutine (genPth ());	
		genPth ();
	}

	private IEnumerator genPth ()
	{
		run = true;
		yield return new WaitForSeconds(0.5f);		
		path = gameObject.GetComponent<My_PathDraw> ();
		ArrayList roadPointsList = path.roadPointsList;
		Transform _wayPoint = (Transform)roadPointsList [0];
		if (GameObject.Find ("ParticleSystem") == null) {
			indicatorT = (Transform)Instantiate (indicatorTest, _wayPoint.position, Quaternion.identity);
			indicatorT.name = "ParticleSystem";
			indicator = indicatorT.gameObject.GetComponent<ParticleSystem> ();
		}
		StartCoroutine (EmitRoutine ());

	}

	IEnumerator EmitRoutine ()
	{
		Transform[] waypoints = path.GetPath ();
		ArrayList roadPointsList = path.roadPointsList;

		while (run) {
			Transform wayPoint = (Transform)roadPointsList [wpCounter];
			if (!wayPoint) {
				yield break;
			}
			float dist = Vector3.Distance (wayPoint.position, indicatorT.position);
			float thisStep = stepDist;
			if (dist < stepDist) {
				thisStep = stepDist - dist;
				indicatorT.position = waypoints [wpCounter].position;
				wpCounter += 1;
				if (wpCounter >= waypoints.Length) {
					wpCounter = 0;
					indicatorT.position = waypoints [wpCounter].position;
				}
			}
			
			if (thisStep > 0) {
				Vector3 pos = new Vector3 (waypoints [wpCounter].position.x, waypoints [wpCounter].position.y, waypoints [wpCounter].position.z);
				Vector3 dir = pos - indicatorT.position;
				if (dir != Vector3.zero) {
					Quaternion wantedRot = Quaternion.LookRotation (dir);

					indicator.startRotation = (wantedRot.eulerAngles.y - 90) * Mathf.Deg2Rad;
					
					indicatorT.LookAt (waypoints [wpCounter]);

					indicatorT.Translate (Vector3.forward * thisStep);
					
					indicator.Emit (1);
				}
			}
			yield return new WaitForSeconds(updateRate*Time.timeScale);
		}
	}
	
	void OnChangeZoom ()
	{
		StopAllCoroutines();
		Destroy (GameObject.Find ("ParticleSystem"));
		wpCounter = 0;
		if (mg.sy_Map.zoom > 14) {
			StartCoroutine (genPth ());	
		}
	}
	
	
	
}



