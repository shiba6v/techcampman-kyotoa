﻿using UnityEngine;
using System.Collections;

public class Points : MonoBehaviour
{

	public double longitude_x;
	public double latitude_y;
	private float objectScale;
	Manager mg;

	void Start ()
	{
		mg = GameObject.Find ("Manager").GetComponent<Manager> (); 
		Zoom.OnChangeZoom += OnChangeZoom;
		objectScale = gameObject.transform.localScale.x;
		double[] point = {longitude_x,latitude_y };
		Vector3 convertPoint = mg.GIStoPos (point);
		Vector3 pos = new Vector3 (convertPoint.x, 0.01f, convertPoint.z);  
		gameObject.transform.position = pos;
	}

	void OnChangeZoom ()
	{
		int zoom = mg.sy_Map.zoom;
		int zoomLevel = mg.sy_Editor.zoom;
		Debug.Log (longitude_x);
		Debug.Log (latitude_y);
		double[] point = {longitude_x,latitude_y };
		Debug.Log (point);
		Vector3 convertPoint = mg.GIStoPos (point);
		Debug.Log (convertPoint);
		Vector3 pos = new Vector3 (convertPoint.x, 0.01f, convertPoint.z);  
		gameObject.transform.position = pos;
		float scalefactor = Mathf.Pow (2f, zoom - zoomLevel);
		transform.localScale = new Vector3 (scalefactor * objectScale, scalefactor * objectScale, scalefactor * objectScale);
	}
}

