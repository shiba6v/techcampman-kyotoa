﻿using UnityEngine;
using System.Collections;
using System;

public class MarkSampleData : MonoBehaviour
{

	public string[] _coordinate;
	
	void Type_A ()
	{
		_coordinate = new string[10] {"-122.467843183633,37.7840132582222",
		                                               "-122.441020446506,37.7742926897645",
		                                               "-122.400259257931,37.7722558352526",
		                                               "-122.489160815105,37.7403069455398",
	                                                   "-122.461635299926,37.7284499383852",
		                                               "-122.426730604067,37.7224280775431",
		                                               "-122.399907868905,37.7441971180824",
		                                               "-122.390303221557,37.7301174448799",
		                                               "-122.424856527079,37.7075102691851",
		                                               "-122.504270562869,37.7828098306149"};		                   
	}

	void Type_B ()
	{
		_coordinate = new string[20] {"-122.493444920915,37.7055030019957",
                                                        "-122.42893770112,37.7033185522398",
                                                        "-122.488048405435,37.6802786654344",
                                                        "-122.405845619437,37.6780934745485",
                                                        "-122.435965724257,37.6711401741737",
                                                        "-122.441236742675,37.7129494872953",
                                                        "-122.462948317047,37.7313142957835",
                                                        "-122.468344832528,37.6577283944983",
                                                        "-122.446884262103,37.6657757540871",
                                                        "-122.402457106732,37.6667691938918",
                                                        "-122.487671900821,37.63984225466",
                                                        "-122.398566596607,37.6437179508997",
                                                        "-122.445754757432,37.6403391468755",
                                                        "-122.487797398867,37.6268224085928",
                                                        "-122.457049796289,37.6240392461867",
                                                        "-122.42805919614,37.6263254218375",
                                                        "-122.401955109312,37.6270212023645",
                                                        "-122.380620030385,37.6236416421614",
                                                        "-122.489052405512,37.6069403860079",
                                                        "-122.453410283565,37.6109172195589"};              
	}

	void Type_C ()
	{
		_coordinate = new string[12] {"-122.373898435951,37.824626752343",
		                                               "-122.36773825837,37.82705964716345",
		                                               "-122.371905439402,37.8197607220866",
		                                               "-122.369550074843,37.8118885157907",
	                                                   "-122.363752257413,37.8107433960911",
		                                               "-122.38639997119,37.7898418742342",
		                                               "-122.379152700057,37.7967142540365",
		                                               "-122.37244897701,37.803156541418",
		                                               "-122.351794257163,37.8186157203515",
		                                               "-122.342010444343,37.8197607138101",
			                                           "-122.326791178173,37.8220506474438",
		                                               "-122.312477816638,37.8253423160218"};		                   
	}
	
	void OnGUI ()
	{
		if (GUI.Button (new Rect (Screen.width * 0.5f, Screen.height * 0.5f - 70, 150, 50), "Type-A")) {
			Type_A ();
			CreateMark ();
		}
		if (GUI.Button (new Rect (Screen.width * 0.5f, Screen.height * 0.5f, 150, 50), "Type-B")) {
			Type_B ();
			CreateMark ();
		}
		if (GUI.Button (new Rect (Screen.width * 0.5f, Screen.height * 0.5f + 70, 150, 50), "Type-C")) {
			Type_C ();
			CreateMark ();
		}
	}
	
	void CreateMark ()
	{
		Manager mg = GameObject.Find ("Manager").GetComponent<Manager> (); 
  
		System.Array.Resize<string> (ref mg.sy_Mark.table_name, 1); 
		System.Array.Resize<string[]> (ref mg.sy_Mark.coordinate, 1); 
		System.Array.Resize<string[]> (ref mg.sy_Mark.field_name, 1); 
		System.Array.Resize<string[]> (ref mg.sy_Mark.field_name, 1); 
		System.Array.Resize<string[][]> (ref mg.sy_Mark.field_contents, 1); 
  
		System.Array.Resize<string> (ref  mg.sy_Mark.coordinate [0], _coordinate.Length);   //resize array
		System.Array.Resize<string> (ref  mg.sy_Mark.field_name [0], _coordinate.Length); 
		System.Array.Resize<string[]> (ref mg.sy_Mark.field_contents [0], _coordinate.Length); 
  
		mg.sy_Mark.table_name [0] = "Mark Test";
  
		for (int i =0; i<_coordinate.Length; i++) {
   
			System.Array.Resize<string> (ref  mg.sy_Mark.field_contents [0] [i], 22); 
   
			mg.sy_Mark.coordinate [0] [i] = _coordinate [i]; // mark coordinate   
			mg.sy_Mark.field_name [0] [i] = "mark" + i; //mark name
			SetInfo (mg, i);
		}
		mg.MarkRefresh ();
	}
	
	void SetInfo (Manager mg, int num)
	{
		mg.sy_Mark.field_contents [0] [num] [2] = "Mark_Green";                //normalSprite
		mg.sy_Mark.field_contents [0] [num] [3] = "Mark_Blue";                  //hoverSprite
		mg.sy_Mark.field_contents [0] [num] [4] = "Mark_Pin_Red";           //activeSprite
		mg.sy_Mark.field_contents [0] [num] [5] = "137";                            //normalColor_r
		mg.sy_Mark.field_contents [0] [num] [6] = "137";                            //normalColor_g
		mg.sy_Mark.field_contents [0] [num] [7] = "137";                             //normalColor_b
		mg.sy_Mark.field_contents [0] [num] [8] = "255";                             //normalColor_a
		mg.sy_Mark.field_contents [0] [num] [9] = "-21";                             //normalPixelInset_x
		mg.sy_Mark.field_contents [0] [num] [10] = "0";                              //normalPixelInset_y
		mg.sy_Mark.field_contents [0] [num] [11] = "64";                           //normalPixelInset_w
		mg.sy_Mark.field_contents [0] [num] [12] = "64";                           //normalPixelInset_h
		mg.sy_Mark.field_contents [0] [num] [13] = "true";                         //textShow
		mg.sy_Mark.field_contents [0] [num] [14] = "32.16";                      //textPixelInset_x
		mg.sy_Mark.field_contents [0] [num] [15] = "43.6";                        //textPixelInset_y
		mg.sy_Mark.field_contents [0] [num] [16] = "28";                           //textFontSize
		mg.sy_Mark.field_contents [0] [num] [17] = "38";                           //textColor_r
		mg.sy_Mark.field_contents [0] [num] [18] = "38";                          //textColor_g
		mg.sy_Mark.field_contents [0] [num] [19] = "38";                          //textColor_b
		mg.sy_Mark.field_contents [0] [num] [20] = "255";                        //textColor_a
		mg.sy_Mark.field_contents [0] [num] [21] = "0";                            //textAlignment
		
	}
	
	
}
