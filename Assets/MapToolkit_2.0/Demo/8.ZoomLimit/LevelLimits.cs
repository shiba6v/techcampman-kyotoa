﻿using UnityEngine;
using System.Collections;

public class LevelLimits : MonoBehaviour
{

	Manager mg;
	int target_level = 13;

	void Awake ()
	{
		mg = GameObject.Find ("Manager").GetComponent<Manager> (); 
		mg.sy_Editor.zoom = 10;
		mg.sy_Map.minLevel = 1;
		mg.sy_Map.maxLevel = 20;
		
	}

	void OnGUI ()
	{
		if (GUI.Button (new Rect (Screen.width * 0.5f, Screen.height * 0.5f, 150, 150), "Min Level : " + mg.sy_Map.minLevel + "\n\n Max Level : " + mg.sy_Map.maxLevel + "\n\n Target Level : " + target_level + "\n\n Current Level : " + mg.sy_Map.zoom)) {
			mg.AutoZoom (target_level, 0, 1f);
		}
	}
}
