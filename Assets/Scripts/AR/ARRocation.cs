﻿using UnityEngine;
using System.Collections;
[CreateAssetMenu]
public class ARRocation : ScriptableObject{

    public double[] lats;
    public double[] longs;

}
