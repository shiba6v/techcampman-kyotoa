﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;
using UniRx;
using UniRx.Triggers;
using SWorker;
using System.Collections.Generic;
using System;
using System.IO;

public class CameraManager : MonoBehaviour {

    [SerializeField]
    private GameObject[] SelectTargets;

    [SerializeField]
    private RenderTexture rendererTexture;

    [SerializeField]
    private GameObject PopUpContent;

    [SerializeField]
    private GameObject _texturePicture;

    [SerializeField]
    private GameObject postPrefab;

    [SerializeField]
    private GameObject ARSet;

    [SerializeField]
    private GameObject ARCanvas;

    [SerializeField]
    private GameObject PopIcon;

    [SerializeField]
    private GameObject[] Characters;

    [SerializeField]
    private GameObject CharacterCanvas;

    [SerializeField]
    private ARRocation rocations;

    private int _characterIndex;
    private List<string> pictures;
    private bool[] GetFlags;
    enum SNSs { twitter,facebook,instagram}
    //Camera　撮影してパスを格納
    
    void CaptureToPNG()
    {
        string time;
        DateTime targetTime = DateTime.Now;
        DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, 0);

		double _lat = GPSGetter.Instance.Lat;
		double _long = GPSGetter.Instance.Long;

		Picture _pic = new Picture ();

        long unixTime = (long)(targetTime - UnixEpoch).TotalSeconds;

        RenderTexture.active = rendererTexture;
        Texture2D tex2d = new Texture2D(900, 1600, TextureFormat.ARGB32, false);
        tex2d.ReadPixels(new Rect(0, 0, rendererTexture.width, rendererTexture.height), 0, 0);
        tex2d.Apply();

        GameObject texturePanel = Instantiate(_texturePicture);
        texturePanel.transform.SetParent(PopUpContent.transform,true);
        texturePanel.GetComponent<RawImage>().texture = tex2d;
        texturePanel.transform.localScale = new Vector3(1,1,1);

        byte[] pngData = tex2d.EncodeToPNG();
        File.WriteAllBytes(time = Application.persistentDataPath + string.Format("{0}.png", unixTime), pngData);
        
        pictures.Add(time);
    }

    public void OnScreenShot()
    {
        CaptureToPNG();
        PopIcon.SetActive(true);
    }

    //投稿プレハブを起こす関数
    public void OnPost()
    {
		postPrefab.SetActive(!postPrefab.active);
    }

    public void CollectionMode()
    {
        CharacterCanvas.SetActive(!CharacterCanvas.active);
        ARSet.SetActive(!ARSet.active);
        ARCanvas.SetActive(!ARCanvas.active);
    }

    public void OnCharacterRight()
    {
        Characters[_characterIndex].SetActive(false);
        do
        {
            _characterIndex++;
            if (_characterIndex == 4)
                _characterIndex = 0;
        } while (!GetFlags[_characterIndex]);
        Characters[_characterIndex].SetActive(true);
    }

    public void OnCharacterLeft()
    {
        Characters[_characterIndex].SetActive(false);
        do
        {
            _characterIndex--;
            if (_characterIndex == -1)
                _characterIndex = 3;
        } while (!GetFlags[_characterIndex]);
            Characters[_characterIndex].SetActive(true);
    }

    private bool GetCharacter()
    {
        if (ChangeModel() == 4)
            return false;
        else
        {
            if (GetFlags[ChangeModel()])
            {
                return false;
            }

            else
            {
                GetFlags[ChangeModel()] = true;

                return true;
            }
        }
    }

    public void MapLoad()
    {
        SceneManager.LoadScene("Map");
    }

    public void OnPostSNS(int value)
    {
        switch (value)
        {
            case (int)SNSs.twitter:
        SocialWorker.PostTwitter("","",pictures[0],null);
                break;

            case (int)SNSs.facebook:
                SocialWorker.PostFacebook(pictures[0], null);
                break;

            case (int)SNSs.instagram:
                SocialWorker.PostInstagram(pictures[0], null);
                break;
        }
        
    }


    private int ChangeModel()
    {
       double gps_lat = GPSGetter.Instance.Lat;
       double gps_long = GPSGetter.Instance.Long;

        const double border = 0.1f;

        for(int i=0;i<3;i++)
        {
            if(Math.Abs(gps_lat - rocations.lats[i])<border && Math.Abs(gps_long - rocations.longs[i]) < border)
            {
                return i;
            }
        }       
            return 3;       
    }

    // Use this for initialization
    void Start () {
        pictures = new List<string>();
        _characterIndex = 3;
        GetFlags = new bool[4];
        GetFlags[3] = true;
        GetCharacter();
	}
	
	// Update is called once per frame
	void Update () {
        Debug.Log(ChangeModel());
            for (int i = 0; i < SelectTargets.Length; i++)
            {
                if (ChangeModel() == i)
                    SelectTargets[i].SetActive(true);
                else
                    SelectTargets[i].SetActive(false);
            }

	}
}

