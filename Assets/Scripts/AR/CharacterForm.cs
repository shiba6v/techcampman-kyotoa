﻿using UnityEngine;

[CreateAssetMenu]
public class CharacterForm : ScriptableObject
{

    public Animator[] animators;
    public GameObject character;

}
