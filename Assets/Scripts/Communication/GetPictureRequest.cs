﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine.UI;

public class GetPictureRequest : MonoBehaviour {

    const string URL = UserLogin.ServerURL + "picture/index";

    [System.Serializable]
    public class PictureAlbumJsonData
    {
        [System.Serializable]
        public class PictureJsonData
        {
            public int id;
            public double latitude;
            public double longitude;
            public string url_icon;
            public string url_image;
        }

        public PictureJsonData[] images;
    }

    public void TestPicture()
    {
        StartGetDownloadPicture(PictureAlbum.Instance.Pictures.ToArray()[0]);
    }

    /** URLから画像のDL*/
    public void StartGetDownloadPicture(Picture picture)
    {
        StartCoroutine(GetDownLoadPicture(picture));
    }
    /** 画像のDLの内部処理*/
    private IEnumerator GetDownLoadPicture(Picture picture)
    {
        string iconPath = picture.URLIcon;
        WWW www = new WWW(iconPath);

        yield return www;

        DateTime targetTime = DateTime.Now;
        DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, 0);

        long unixTime = (long)(targetTime - UnixEpoch).TotalSeconds;
 
        File.WriteAllBytes(Application.persistentDataPath + picture.Id.ToString() + unixTime.ToString(),www.bytes);
        picture.SetPathIcon(Application.persistentDataPath + picture.Id.ToString() + unixTime.ToString());

        Debug.Log("Download Finished");
        yield break;

    }
    /** サーバーからのGETの開始*/
     public void StartGetRequest()
    {
        StartCoroutine(GetRequest());
    }

    /** GETの内部処理*/
    public static IEnumerator GetRequest()
    {
        Dictionary<string, string> header = new Dictionary<string, string>();
        var guid = PlayerPrefs.GetString("id");
        header.Add("user_id", guid);
        WWW request = new WWW(URL, null , header);
        yield return request;
        if (string.IsNullOrEmpty(request.error))
        {
            Debug.Log(request.text);
            PictureAlbumJsonData albumData = JsonUtility.FromJson<PictureAlbumJsonData>(request.text);
            Debug.Log(albumData.images[0].id);
            Picture.Create(albumData);
            
        }
        else
        {
            Debug.Log(request.error);
        }
    }



}
