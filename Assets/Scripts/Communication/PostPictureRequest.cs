﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine.UI;

public class PostPictureRequest : MonoBehaviour
{


    const string URL = UserLogin.ServerURL + "picture";
    
        [System.Serializable]
        public class PictureJsonData
        {
            public double latitude;
            public double longitude;
            public byte[] byte_image;
        }
        

    public void TestPost()
    {
        Debug.Log(Player.Instance.Pictures.Count);
        Picture picture = PictureAlbum.Instance.Pictures.ToArray()[0];
        //今回testなので、IconをImageにして扱う。
        picture.SetPathImage(picture.PathIcon);
        StartPostRequest(picture);
    }


    /** ポストの開始*/
    public void StartPostRequest(Picture picture)
    {
        Debug.Log("StartGetRequest");
        StartCoroutine(PostRequest(picture));
    }

    /** ポストの内部処理*/
    public IEnumerator PostRequest(Picture picture)
    {
        Dictionary<string, string> header = new Dictionary<string, string>();
        var guid = PlayerPrefs.GetString("id");
        header.Add("user_id", guid);
        WWWForm form = new WWWForm();
        form.AddField("latitude", picture.Lat.ToString());
        form.AddField("longitude", picture.Long.ToString());
        form.AddBinaryData("byte_image", File.ReadAllBytes(picture.PathImage));

        foreach (System.Collections.Generic.KeyValuePair<string,string> entry in form.headers)
        {
            header[System.Convert.ToString(entry.Key)] = System.Convert.ToString(entry.Value);
        }


        WWW request = new WWW(URL,form.data,header);
        yield return request;
        if (string.IsNullOrEmpty(request.error))
        {
            Debug.Log(request.text);
        }
        else
        {
            Debug.Log(request.error);
        }
    }



}
