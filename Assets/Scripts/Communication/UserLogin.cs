﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UserLogin : MonoBehaviour {

    public const string ServerURL = "http://52.197.204.140:3000/";
    public static UserLogin Instance  { get; private set; }
    void Awake ()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad (gameObject);
        }
        else
        {
            Destroy (gameObject);
        }
    }

    void Start ()
    {
        if(!PlayerPrefs.HasKey("id"))
        {
            StartCoroutine(SignUp());
        }
        else
        {
            Debug.Log(string.Format("Signing up was skipped. user id:{0} ", PlayerPrefs.GetString("id")));
        }
	}



    IEnumerator SignUp()
    {
        string url = ServerURL + "sign_up";
        yield return SignUpRequest(url);
    }



    [System.Serializable]
    public class PlayerJsonData
    {
        public int id;
    }

    IEnumerator SignUpRequest(string url)
    {
        Dictionary<string, string> header = new Dictionary<string, string>();
        WWW request = new WWW(url, null , header);
        yield return request;
        if (string.IsNullOrEmpty(request.error))
        {
            Debug.Log(request.text);
            PlayerJsonData data = JsonUtility.FromJson<PlayerJsonData>(request.text);
            Player.Instance.SetId(data.id);
            PlayerPrefs.SetString("id", data.id.ToString());
            Debug.Log(string.Format("User has reated. user id:{0}", PlayerPrefs.GetString("id")));
        }
        else
        {
            Debug.Log(request.error);
        }

    }


	
}
