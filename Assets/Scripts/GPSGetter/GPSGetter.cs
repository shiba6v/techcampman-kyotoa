﻿using UnityEngine;
using System.Collections;
using UniRx;

public class GPSGetter : MonoBehaviour {
    /** GPSGetter.Instanceを使うとどの場所からでも自分が呼べる */
	public static GPSGetter Instance  { get; private set; }
	void Awake ()
	{
		if (Instance == null)
		{
			Instance = this;
			DontDestroyOnLoad (gameObject);
		}
		else
		{
			Destroy (gameObject);
		}
	}
    /** 緯度 */
    public double Lat { get; private set; }
    /** 経度 */
    public double Long  { get; private set; }

	IEnumerator Start(){


		if (!Input.location.isEnabledByUser) {
		}
		Input.location.Start ();

		int maxWait = 20;
		while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0) {
			yield return new WaitForSeconds (0.3f);
			maxWait--;
		}
		if (maxWait < 1) {
			print ("Timed out");
		}
		if (Input.location.status == LocationServiceStatus.Failed) {
			print ("Unable to determine device location");
		} else {
            /** GPSの取得を1.5秒毎に行う*/
            Observable.Interval(System.TimeSpan.FromSeconds(60f))
                .Subscribe(_ => GetGPS())
                .AddTo(gameObject);

		}

		//Input.location.Stop ();
	}
    /** GPSの取得*/
    void GetGPS()
    {
        Long = double.Parse(Input.location.lastData.longitude.ToString());
        Lat = double.Parse(Input.location.lastData.latitude.ToString());
//		Long = 135.729243;
//		Lat = 35.03937;
    }
}
