﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GPS : MonoBehaviour
{
	[SerializeField]
	Text _debugText;

	private GUIText gps_test;
	private Transform target;
	double Lat;
	double Long;
	Manager mg;

	void Start ()
	{
		mg = GameObject.Find ("Manager").GetComponent<Manager> (); 
		target = new GameObject ("target").transform;

		double x = GPSGetter.Instance.Long;
		double y = GPSGetter.Instance.Lat;

		GPSCenter (x, y);
		DisplayUI (x, y);
	}

	void Update(){

		double beforeLong = Long;
		double beforeLat = Lat;
		
		Long = GPSGetter.Instance.Long;
		Lat = GPSGetter.Instance.Lat;
		_debugText.text = string.Format ("Lat: {0}, Long:{1}", Lat, Long);
		if (beforeLong != Long || beforeLat != Lat) {
			GPSCenter (Long, Lat);
			DisplayUI (Long, Lat);
		}
	}

	void GPSCenter (double x, double y)
	{
		target.position = mg.GIStoPos (new double[] {x, y});
//		Debug.Log (target.position);
		mg.SetMarkTarget (target);
		mg.RunMarkMove ();

	}

	void DisplayUI (double x, double y)
	{
		if (GameObject.Find ("GPS_Test") == null) {
			gps_test = new GameObject ("GPS_Test").AddComponent (typeof(GUIText))as GUIText;
		} else {
			gps_test = GameObject.Find ("GPS_Test").GetComponent<GUIText> ();
		}
//		gps_test.text = "GPS Coordinate: " + "x: " + x + " ,  y:  " + y + "\n\n" + "altitude: " + Input.location.lastData.altitude + "\n\n" + "horizontalAccuracy: " + Input.location.lastData.horizontalAccuracy + "\n\n" + "Time Stamp: " + Input.location.lastData.timestamp; 
		gps_test.text = "";
		gps_test.anchor = TextAnchor.MiddleCenter;
		gps_test.alignment = TextAlignment.Center;
		gps_test.pixelOffset = new Vector2 (Screen.width / 2, Screen.height / 2);
		gps_test.material.color = Color.red;
		gps_test.fontSize = 20;	

	}
}