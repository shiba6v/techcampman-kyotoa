﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Map : MonoBehaviour {
    [SerializeField]
    Text _debugText;
	public GameObject flag;

    /** マップのピンが押された時に呼ばれて、MapInfoPopupを開く */
    public void OnPressedMapPin(Picture picture)
    {
        
    }

    /** Camera.unityへのシーン切り替え */
    public void OnPressedCameraBtn()
    {
        
    }

    /** ピンの位置を更新 */
    void Refresh()
    {
        SetPin();
    }

    /** とりあえず、ローカルのPictureモデルをよみこんで、Map上に置いていく */
    void SetPin()
    {
        
    }

    void Update()
    {
//        _debugText.text = GPSGetter.Instance.debugText;
    }



}
