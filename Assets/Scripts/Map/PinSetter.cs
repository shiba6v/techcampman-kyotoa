﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.IO;
//using UnityEngine.Renderer;

public class PinSetter : MonoBehaviour {
	[SerializeField]
	GameObject _pinPrefab;
	List<GameObject> _pinsList = new List<GameObject>();
	[SerializeField]
	GameObject _plane;

	Manager mg;
	private Transform _target;
	private Transform _target2;
	private float _objectScale;
	public double _pinLong;
	public double _pinLat;
	private float _num;
	private int _listNum;
	static Vector3 flagPosOffset = new Vector3 (0f, 0.1f, 0f);
	private PinTexture pt;

	void Start(){
		mg = GameObject.Find ("Manager").GetComponent<Manager> (); 
		_target = new GameObject ("target").transform;
		_target2 = new GameObject ("target").transform;
		_listNum = 0;
		pt = GetComponent<PinTexture>();

		Picture pic = GetComponent<Picture> ();
		pic.SetPathIcon (Application.dataPath + "/sample.png");

//		pinMark (pic.SetPicture (135.777, 35.02487));
	}

	void Update(){
//		Zoom.OnChangeZoom += OnChangeZoom;
//		_objectScale = gameObject.transform.localScale.x;
	}
		
//	void FixedUpdate(){
//
//		double x = GPSGetter.Instance.Long;
//		double y = GPSGetter.Instance.Lat;
//
//		Vector3 GPSPos = SetFlag (x, y, _listNum);
//
//		_pinsList.Add (GeneratePin(GPSPos));
////		_num += 2f;
//		flagPosOffset = new Vector3 (_num, 0.5f, 0f);
//		_listNum++;
//
//	}
//
//
//	Vector3 SetFlag(double x, double y, int i){
//
//		_target.position = mg.GIStoPos (new double[] {x, y});
//		var pos = _target.position + flagPosOffset;
//
//		return pos;
//	}
 
	public void pinMark(Picture setPicture){

		_pinLong = setPicture.Long;
		_pinLat = setPicture.Lat;
		string icon = setPicture.PathIcon;
		byte [] iconByte = File.ReadAllBytes (icon);
		Texture2D texture = new Texture2D (100, 100);
		texture.LoadImage (iconByte);

		double[] point = { _pinLong, _pinLat };
		_target.position = mg.GIStoPos(point);
		var pos = _target.position + flagPosOffset;
		_pinsList.Add (GeneratePin(pos, texture));

//		for (int i = 0; i < _pinsList.Count; i++) {
//
//			double[] point = { _pinLong, _pinLat };
////			Vector3 convertPoint = mg.GIStoPos (point);
////			Vector3 pos = new Vector3 (convertPoint.x, 0.01f, convertPoint.z);
//			_target.position = mg.GIStoPos(point);
//			var pos = _target.position + flagPosOffset;
//
//			var vec = _pinsList [i].transform;
//			var newPos = vec.position;
//			var x = newPos.x - pos.x;
//			var z = newPos.z - pos.z;
//
////			if ( -5f < x < 5f || -5f < z < 5f) {
//				_pinsList.Add (GeneratePin(pos, texture));
////			}
//		}
	}

	public GameObject GeneratePin(Vector3 pos, Texture2D icon){
//	public GameObject GeneratePin(Vector3 pos){
		GameObject pinInstances = (GameObject)Instantiate (
			_pinPrefab,
			pos,
			_pinPrefab.transform.rotation
		);
		pinInstances.transform.SetParent (transform, false); 

		GameObject[] planes = new GameObject[pinInstances.transform.childCount];
		for (int i = 0; i < pinInstances.transform.childCount; i++) {
			
			planes [i] = pinInstances.transform.GetChild (i).gameObject;
			Renderer rend = planes [i].GetComponent<Renderer> ();
			Material mat = rend.material;
			Texture texture = mat.mainTexture;
			texture = icon;

			pinInstances.transform.GetChild (i).gameObject.GetComponent<Renderer> ().material.mainTexture = texture;
		}

		Zoom.OnChangeZoom += OnChangeZoom;
		_objectScale = gameObject.transform.localScale.x;

		return pinInstances;
	}

	void OnChangeZoom ()
	{
		int zoom = mg.sy_Map.zoom;
		int zoomLevel = mg.sy_Editor.zoom;
		double[] point = {_pinLong ,_pinLat };
		Debug.Log (_pinLong);
		Debug.Log (_pinLat);

		_target2.position = mg.GIStoPos (point);
		Debug.Log (_target2.position);
		Vector3 pos = _target2.position + flagPosOffset;  
		Debug.Log (pos);
		gameObject.transform.position = pos;
		float scalefactor = Mathf.Pow (2f, zoom - zoomLevel);
		transform.localScale = new Vector3 (scalefactor * _objectScale, scalefactor * _objectScale, scalefactor * _objectScale);
	}

}
