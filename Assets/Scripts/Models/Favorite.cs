﻿using UnityEngine;
using System.Collections;

public class Favorite : MonoBehaviour {
    public User User { get; private set; }
    public Picture Picture { get; private set; }
    public void SetFavorite(User user, Picture picture)
    {
        User = user;
        Picture = picture;
    }
}
