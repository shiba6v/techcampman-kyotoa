﻿using UnityEngine;
using System.Collections;
using System.IO;

public class Picture : MonoBehaviour {

    public int Id { get; private set; }
    /** 緯度 */
    public double Lat { get; private set; }
    /** 経度 */
    public double Long { get; private set; }
    /** アイコンの外部URL */
    public string URLIcon { get; private set; }
    /** 写真のの外部URL */
    public string URLImage { get; private set; }

    /** アイコンのパス */
    public string PathIcon { get; private set; }
    /** 写真ののパス */
    public string PathImage { get; private set; }

    public User User { get; private set; }

    public static void LoadPref()
    {

        if (PlayerPrefs.HasKey("PictureCount"))
        {
            int _count = PlayerPrefs.GetInt("PictureCount");
            Picture picture = new Picture();
            string idPrefix;
            for (int i = 0; i < _count; ++i)
            {
                idPrefix = (i+1).ToString();
                picture.Set(
                    PlayerPrefs.GetInt(idPrefix + "id"),
                    PlayerPrefs.GetFloat(idPrefix + "lat"),
                    PlayerPrefs.GetFloat(idPrefix + "long"), 
                    PlayerPrefs.GetString(idPrefix + "URLIcon"),
                    PlayerPrefs.GetString(idPrefix + "URLImage")
                    );
                if(PlayerPrefs.HasKey(idPrefix + "PathIcon"))
                {
                    picture.SetPathIcon(idPrefix + "PathIcon");
                }

                if (PlayerPrefs.HasKey(idPrefix + "PathImage"))
                {
                    picture.SetPathImage(idPrefix + "PathImage");
                }

                PictureAlbum.Instance.AddPicture(picture);
            }

        }
    }

    public static void Create(GetPictureRequest.PictureAlbumJsonData albumData)
    {
       
        bool idCheck = false;
        foreach(GetPictureRequest.PictureAlbumJsonData.PictureJsonData data in albumData.images)
        {
            Picture picture = new Picture();
            picture.Set(data);
            foreach(Picture _picture in PictureAlbum.Instance.Pictures)
            {
                if (_picture.Id == data.id)
                    idCheck = true;
            }
            if (!idCheck)
            {
                PictureAlbum.Instance.AddPicture(picture);
                Debug.Log(string.Format("PicutreAdded id: {0}", data.id));
                picture.SetPref(PlayerPrefs.GetInt("PictureCount"), data);
            }
        }

    }

    public void SetPref(int picturePosition, GetPictureRequest.PictureAlbumJsonData.PictureJsonData data)
    {
        string idPrefix = PlayerPrefs.GetInt("PictureCount").ToString();

        PlayerPrefs.SetInt(idPrefix + "id", data.id);
        PlayerPrefs.SetFloat(idPrefix + "lat", (float)data.latitude);
        PlayerPrefs.SetFloat(idPrefix + "long", (float)data.longitude);
        PlayerPrefs.SetString(idPrefix + "URLIcon", data.url_icon);
        PlayerPrefs.SetString(idPrefix + "URLImage", data.url_image);

        Debug.Log(string.Format("{0}:id,{1}:lat,{2}:long,{3}:lat,{4}:URLIcon,{5}:URLImage",data.id ,data.latitude,data.longitude,data.url_icon,data.url_image));
    }

    public void Set(GetPictureRequest.PictureAlbumJsonData.PictureJsonData data)
    {
        Id = data.id;
        Lat = data.latitude;
        Long = data.longitude;
        URLIcon = data.url_icon;
        URLImage = data.url_image;
    }

    public void Set(int _id,double _lat,double _long,string _URLIcon,string _URLImage)
    {
        Id = _id;
        Lat = _lat;
        Long = _long;
        URLIcon = _URLIcon;
        URLImage = _URLImage;
    }

    /** アイコンパスのセット*/
    public void SetPathIcon(string path)
    {
        PathIcon = path;
    }

    /** 写真パスのセット*/
    public void SetPathImage(string path)
    {
        PathImage = path;
    }

	public Picture SetPicture(double Lat, double Long){

		Picture pic = new Picture();
		pic.Lat = Lat;
		pic.Long = Long;

		pic.SetPathIcon (Application.dataPath + "/sample.png");
		Texture2D texture = new Texture2D(100, 100);
		var  a = File.ReadAllBytes (Application.dataPath + "/Image1.png");
		texture.LoadImage(a);
		Debug.Log (texture);
		byte[] icon = texture.EncodeToPNG();
		File.WriteAllBytes (pic.PathIcon, icon);

		return pic;
	}

    /** URLIcon,URLImageからダウンロードして保存し、そのPathをPathIcon,PathImageに保存するように書いてほしいです */











}
