﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PictureAlbum : MonoBehaviour {
    public static PictureAlbum Instance { get; private set; }
    void Awake ()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad (gameObject);
        }
        else
        {
            Destroy (gameObject);
        }
    }

    public List<Picture> Pictures { get; private set; }
    public  void AddPicture(Picture picture)
    {
        if (!PlayerPrefs.HasKey("PictureCount"))
        {
            PlayerPrefs.SetInt("PictureCount", 0);
        }
        int _count = PlayerPrefs.GetInt("PictureCount");

        PlayerPrefs.SetInt("PictureCount", _count + 1);
        PictureAlbum.Instance.Pictures.Add(picture);

    }

}
