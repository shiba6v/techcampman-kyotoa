﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/** UI関連やサーバーに関係ないものは全てこっちに書く */
public class Player : User {
    /** Player.Instanceを使うとどの場所からでも自分が呼べる */
    public static Player Instance { get; private set; }
    void Awake ()
    {
        if (Instance == null)
        {
            Instance = this;
            Favorites = new List<Favorite>();
            Pictures = new List<Picture>();
            Characters = new List<Character>();
            DontDestroyOnLoad (gameObject);
        }
        else
        {
            Destroy (gameObject);
        }
    }
    /** picutreに対してファボをつける。 */
    public void SetFavorite(Picture picture)
    {
        Favorite favorite = new Favorite();
        favorite.SetFavorite(this, picture);
        Favorites.Add(favorite);
    }

}
