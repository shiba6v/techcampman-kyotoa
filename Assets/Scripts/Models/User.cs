﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/** サーバーから持ってこない情報はPlayerに書く */
public class User : MonoBehaviour {
    public string Name { get; protected set; }
    public int id { get; protected set; }
    public int point { get; protected set; }
    public List<Favorite> Favorites { get; protected set; }
    public List<Picture> Pictures { get; protected set; }
    public List<Character> Characters { get; protected set; }

    public void SetId(int val)
    {
        id = val;
    }

}
