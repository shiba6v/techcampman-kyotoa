﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraSharePopup : MonoBehaviour {
    [SerializeField]
    GameObject _picturePrefab;


    /** この上にPicutreを乗せていく。 */
    [SerializeField]
    Transform _pictureRoot;
   

    /** _pictureRootに表示しているPicture */
    public List<PicturePanel> PicutrePanels { get; private set;}


    /* GameObject.Instantiateなどのあとに呼ぶ。 */
    public void SetPictures(List<Picture> pictures)
    {
        //picturesからPicutrePanelのインスタンスを作ってListにいれる。

        //_picturePrefabのインスタンスを作って_pictureRootにおく。

    }

    /** PicturePanelから削除。Picutresを他クラスからいじらずに、このメソッドを通して削除する。選択されたPicutreを消す */
    void DeletePicture()
    {

    }
        
    /** スタンプ加工ボタンを押された時 */
    public void OnPressedStampBtn()
    {
        
    }

    /** 保存ボタンを押された時 */
    public void OnPressedSaveBtn()
    {
        
    }

    /** twitterシェアボタン */
    public void OnPressedTwitterBtn()
    {

    }

    /** facebookシェアボタン */
    public void OnPressedFacebookBtn()
    {

    }
    /** Instagramシェアボタン */
    public void OnPressedInstagramBtn()
    {

    }

    /** リストから削除ボタン */
    public void OnPressedDeleteBtn()
    {
        DeletePicture();
    }

    /** 閉じるボタン */
    public void OnPressedCloseBtn()
    {
        
        SharePictures();
    }

    /** _imagePopup.Picutresに入っているものを削除する */
    public void SharePictures()
    {
        
    }

}
