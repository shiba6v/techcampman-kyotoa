﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapInfoPopup : MonoBehaviour {

    [SerializeField]
    GameObject _picturePrefab;

    /** この上にPicutreを乗せていく。 */
    [SerializeField]
    Transform _pictureRoot;

    /* GameObject.Instantiateなどのあとに呼ぶ。 */
    public void SetPictures(List<Picture> pictures)
    {
        //picturesからPicutrePanelのインスタンスを作ってListにいれる。

        //_picturePrefabのインスタンスを作って_pictureRootにおく。

    }

    /** PicturePanelから削除。Picutresを他クラスからいじらずに、このメソッドを通して削除する。選択されたPicutreを消す */
    public void DeletePicture()
    {

    }


    
}
